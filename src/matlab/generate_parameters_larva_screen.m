function parameters = generate_parameters_larva_screen(rig, varargin)

list_designation = {'pipeline','neuron','stimuli','protocol', 'id', 'numero_larva', 't'};

list_behaviour = {'straight', 'bend', 'curl', 'ball', ...
    'compressing', 'expanding','nematicking', 'denematicking',...
    'outbending', 'inbending',...
    'run','cast','stop','back','roll', 'head_wiggle', 'wiggling', 'hunch','digging','roll_first_instar'};

% [FL:] no longer rig dependent
list_rig_dependent_behaviour = {'pure_cast', 'global_cast','strong_cast',...
    'global_run', 'strong_hunch', 'head_hunch', 'run_cast'};

threshold_strong_cast_on_S                                = 0.8;
threshold_strong_hunch_on_normalized_midline              = 0.81;
threshold_strong_hunch_eig                                = 0.68;
threshold_relative_velocity                               = 1.5;
threshold_moving_back                                     = -0.8;


parameters.list_designation                               = list_designation;
parameters.list_behaviour                                 = list_behaviour;
parameters.list_rig_dependent_behaviour                   = list_rig_dependent_behaviour;
parameters.threshold_strong_cast_on_S                     = threshold_strong_cast_on_S;
parameters.threshold_strong_hunch_on_normalized_midline   = threshold_strong_hunch_on_normalized_midline;
parameters.threshold_strong_hunch_eig                     = threshold_strong_hunch_eig;
parameters.threshold_relative_velocity                    = threshold_relative_velocity;
parameters.threshold_moving_back                          = threshold_moving_back;


% [FL:] apparently, the below parameters are not used in pipeline_extract_states;
% the present file might have been copied from a downstream component of the extended pipeline

%[deleted code]

%parameters.t_min_normalize = t_min;
%parameters.t_max_normalize = t_max;
%parameters.t_stimuli_min   = t_stimuli_min;
%parameters.t_stimuli_max   = t_stimuli_max;

%parameters.t_before_stimuli       = parameters.t_stimuli_min - dt_before;
%parameters.t_after_stimuli        = parameters.t_stimuli_min + dt_after;
%parameters.t_short_after_stimuli  = parameters.t_stimuli_min + dt_short_after;

end
