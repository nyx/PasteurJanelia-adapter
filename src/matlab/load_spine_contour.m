function trx= load_spine_contour(outline_loc, rig)

dir_path  = outline_loc.folder;
file_name = outline_loc.name;
file_stem = file_name(1:end-8);
file_path = [dir_path '/' file_name];

try
    [protocol, pipeline, stimuli, neuron] = process_names(file_path);
catch
    disp(['WARNING: failed to identify metadata in file stem: ' file_stem])
    protocol = 'NA';
    if (nargin<2)
        pipeline = 'NA';
    else
        pipeline = rig;
    end
    stimuli = 'NA';
    neuron = 'NA';
end

dx = 1;
dy = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[numero_exp, ~ ,numero_traj,longueur_numero_traj,state] = maximal_index();

spine_data = load([dir_path '/' file_stem '.spine']);

[name, numero_traj_contour, t_contour,  contour_x, contour_y] = transform_load_outline_files(file_path);
[numero_traj_common] = get_common_numero_traj(spine_data(:,2) ,numero_traj_contour);
n_numero_traj_common = length(numero_traj_common);


%for i = 1 : n_numero_traj_common

for i = n_numero_traj_common : -1 : 1
    results(i,1).full_path          = dir_path;
    results(i,1).id                 = numero_exp; 
    results(i,1).numero_larva       = num2str( numero_traj_common(i));
    results(i,1).numero_larva_num   = numero_traj_common(i);
    results(i,1).protocol           = protocol;
    results(i,1).pipeline           = pipeline;
    results(i,1).stimuli            = stimuli;
    results(i,1).neuron             = neuron;

    t_loc                           = spine_data(:,3);
    II_chore                        = find( spine_data(:,2) == numero_traj_common(i)   ); 
    II_contour                      = find( numero_traj_contour == numero_traj_common(i) );
    [poule, I_chore, I_contour]     = intersect(t_loc(II_chore), t_contour(II_contour) );
    II_select_chore                 = II_chore(I_chore);
    II_select_contour               = II_contour(I_contour);
    results(i,1).t                  = spine_data(II_select_chore,3);
    results(i,1).full_t             = unique(spine_data(:,3));

    results(i,1).x_spine            = spine_data(II_select_contour,4:2:25)*dx;
    results(i,1).y_spine            = spine_data(II_select_contour,5:2:25)*dy;
    results(i,1).x_contour          = contour_x(II_select_contour,:)*dx ;
    results(i,1).y_contour          = contour_y(II_select_contour,:)*dy ;

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
trx = results;


end
