function [name, Id, t,  contour_x, contour_y] = transform_load_outline_files(outline_file)
%%
max_lateral_size = 500;
%%

lines = readlines(outline_file);

if isempty(lines{end,1})
    lines = lines(1:end-1,1);
end

nn = length(lines);

% [FL:] `name` is supposed to be a numerical array of truncated dates, but is not used in practice
name = cell(nn,1);
% [FL:] `Id` is also expected to be a numerical array
Id = zeros(nn,1);
t = zeros(nn,1);

contour_y = nan(nn,max_lateral_size);
contour_x = nan(nn,max_lateral_size);

for i =1 : nn 
    line = lines(i);
    values = split(line, " ");
    name{i} = values(1);
    Id(i) = str2double(values(2));
    t(i) = str2double(values(3));
    poule = str2double(values(4:end));
    kk = length(poule) / 2;
    
    x_contour       = poule(1:2:end);    
    x_contour       = gaussian_smooth_choreography(x_contour, 5);
    x_contour       = [x_contour; x_contour(1)];
    
    y_contour       = poule(2:2:end);
    y_contour       = gaussian_smooth_choreography(y_contour, 5);
    y_contour       = [y_contour; y_contour(1)];
   
    
%     fprintf('%i\t %i\t %i\t %i\t %i\n', i, nn,length(x_contour), length(y_contour), kk);
    try
        contour_x(i,1:kk+1) = x_contour';
        contour_y(i,1:kk+1) = y_contour';
    catch
        fprintf('error handled in transform_load_outline_files \n');
        
        n_x_contour = length(x_contour);
        n_y_contour = length(y_contour);
        n_contour   = min(n_x_contour, n_y_contour);
%         fprintf('%i\t %i\t %i\n', n_x_contour,n_y_contour,n_contour); 
        contour_x(i,1:n_contour) = x_contour(1:n_contour)';
        contour_y(i,1:n_contour) = y_contour(1:n_contour)';
        
    end
%     contour{i,1}.x  = poule(1:2:end);
%     contour{i,1}.y  = poule(2:2:end);
%     contour{i,2}.id = Id(i);
end




end
