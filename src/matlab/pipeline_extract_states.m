function pipeline_extract_states(path_to_classifiers, target_folder, rig, varargin)
warning off;

%% parameters linked to rig
if (nargin<3)
    rig = 'NA';
end
parameters               = generate_parameters_larva_screen(rig);


path_to_classifiers_1      = [ path_to_classifiers 'Classifier_first_layer_RF_compact/' ];
path_to_classifiers_2      = [ path_to_classifiers 'Classifier_second_layer_RF_compact/' ];
path_to_classifiers_3      = [ path_to_classifiers 'Classifier_third_layer_RF_compact/' ];
path_to_classifiers_4      = [ path_to_classifiers 'Classifier_fourth_layer_RF_compact/' ];
path_to_classifiers_5      = [ path_to_classifiers 'Classifier_fifth_layer_RF_compact/' ];


liste_to_keep = generate_liste_of_features_to_keep();

files                        = dir([target_folder '/*.outline']);

mode                         = 'all';

    
compute_trx_5_layers_force_output_trx(rig,files(1),mode,liste_to_keep,parameters,path_to_classifiers_1,...
    path_to_classifiers_2,path_to_classifiers_3,path_to_classifiers_4,path_to_classifiers_5)


end
