function compute_trx_5_layers_force_output_trx(rig,file_loc,mode,liste_to_keep,parameters,...
    path_to_classifiers_1,path_to_classifiers_2,path_to_classifiers_3,path_to_classifiers_4,path_to_classifiers_5,...
    print_txt_files)

%% There are 5 stages in behavioural idenfitication %%
%% Genetic lines used for tagging were chosen (after the first stage) %%
%% lines are different depending on the stage as they were selected %%
%% At each round new lines were selected from the total classification   %%
%% of the screen %% 
%% list of lines 
%%  MZZ_R_3013849@UAS_Chrimson_Venus_X_0070; BJD_116H11_AE_01@UAS_Chrimson_Venus_X_0070; BJD_SS02401@UAS_Chrimson_Venus_X_0070
%%  GMR_17F10_AE_01@UAS_Chrimson_Venus_X_0070; GMR_72F11_AE_01@UAS_Chrimson_Venus_X_0070; GMR_72F01_AE_01@UAS_Chrimson_Venus_X_0070
%%  from t5 FCF_attP2_1500062@UAS_TNT_2_0003, FCF_attP2_1500062@UAS_TNT_2_0003

    if (nargin<11)
        print_txt_files = false;
    end

    cd(file_loc.folder);

    trx = load_spine_contour(file_loc);
    trx = clean_larvae(trx);
    trx = recut_contours_500(trx);
    
    if (strcmp(rig, 'NA') && ~strcmp(trx(1).pipeline, 'NA'))
        rig = trx(1).pipeline;
	disp(['INFO: rig name extracted from file name: ' rig])
    end
         
    if isempty(trx)
        
    else
        
        fprintf('estimators\n');
        
        trx = generate_minimal_estimators_full_choreography(trx, rig);
        fprintf('first layer\n');
        
        trx = give_first_layer_all_states_choreography(trx, path_to_classifiers_1, parameters);

        fprintf('second layer\n');
        trx = give_second_layer_empiral_rules(trx); %% usefull for old estimations, useless now but quick;

        trx = give_second_layer_all_states_choreography(trx, path_to_classifiers_2, mode);
        fprintf('third layer\n');
        trx = regularize_singular_events(trx); %% regularize patterns: 101; 123  
        
        trx = give_third_layer_all_states_choreography(trx, path_to_classifiers_3);
        fprintf('fourth layer\n');
        trx = give_larva_length_pure(trx);
        
        trx = fourth_layer_all_behaviour_correction(trx,rig, path_to_classifiers_4);
        fprintf('fifth layer\n');
        
        trx = fifth_layer_reintroduce_roll(trx,rig, path_to_classifiers_5);
        fprintf('post processing\n');
        trx = post_process_trx(trx, liste_to_keep);

	if print_txt_files
            print_pure_state_and_selected_amplitude(trx, rig);
            print_pure_state_and_selected_amplitude_state_large(trx, rig);
            print_pure_state_and_selected_amplitude_state_large_small(trx, rig);
	end%if

        
        save(['trx.mat'], 'trx', '-v7.3');    
        try 
           delete('*.contour');
        end; 
         
        
    end
    
    
end
