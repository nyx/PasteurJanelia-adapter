#!/bin/sh

# set MatLab
if ! command -v mcc >/dev/null; then
  module load matlab # latest available version
fi
# compile
dir=`dirname $1`
file=`basename $1 .m`
(cd "$dir" && mcc -m -v "$file.m")
# clean up to avoid permission-denied issues on next compilation
(cd "$dir" && rm -f "run_$file.sh" includedSupportPackages.txt mccExcludedFiles.log readme.txt requiredMCRProducts.txt unresolvedSymbols.txt)
