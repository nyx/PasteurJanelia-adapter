The present sub-package builds the pipeline\_extract\_states\_2023b program made available as [an artifact](https://gitlab.pasteur.fr/nyx/artefacts/-/tree/master/PasteurJanelia).

The pipeline\_extract\_states program is assembled from various sources, and then compiled using mcc on Maestro (currently R2023b).
The resulting binary is then manually uploaded so that a regular PasteurJanelia-adapter installation gets that binary and runs it using the MatLab Runtime Engine which does no require a MatLab license.

For the major part, the code originates from [pipeline\_extract\_states.tgz](https://gitlab.pasteur.fr/nyx/artefacts/-/tree/master/PasteurJanelia).
Some source files are overwritten with those present in this repository, in order to remove the "pipeline" part (remove the [copy\_perl\_program\_evrywhere\_screen](https://github.com/DecBayComp/Pipeline_action_analysis_t5_pasteur_janelia/tree/master/copy_perl_program_evrywhere_screen) and [give\_path\_for\_pipeline\_extract\_states](https://github.com/DecBayComp/Pipeline_action_analysis_t5_pasteur_janelia/tree/master/give_path_for_pipeline_extract_states) dependencies), have pipeline\_extract\_states take a single behavioral assay as input, as well as to relax some expectations on the names of the input files.

The main difference between this version and [Pipeline_action_analysis_t5_pasteur_janelia](https://github.com/DecBayComp/Pipeline_action_analysis_t5_pasteur_janelia/tree/master/pipeline_extract_states) is a fifth "layer" of classifiers that re-introduce the *roll* behavior into the various dictionaries of actions.
As a consequence, the weight files of the classifiers are also fetched from two different sources.
The first four layers originate from [Pipeline_action_analysis_t5_pasteur_janelia](https://github.com/DecBayComp/Pipeline_action_analysis_t5_pasteur_janelia), while the fifth has been made available as [another artifact](https://gitlab.pasteur.fr/nyx/artefacts/-/tree/master/PasteurJanelia).

As stated [elsewhere](https://gitlab.pasteur.fr/nyx/artefacts/-/tree/master/PasteurJanelia), the code originates from a copy actively used on the Maestro HPC cluster at Institut Pasteur, mostly used by Tihana Jovanic and her colleagues.
In principle, the same version was used to generate all the *trx.mat* files in the *t5* and *t15* directory in */pasteur/zeus/projets/p02/hecatonchire/screens* (internal to Institut Pasteur), although MatLab had to be updated several times in the meantime.
