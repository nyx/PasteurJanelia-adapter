import logging

def make_dataset(backend, **kwargs):
    """
    Trivially links the input .spine/.outline files from *raw* into *interim*.
    """
    for spine_file in backend.raw_data_dir().glob('*.spine'):
        logging.info(f'found file: {spine_file.relative_to(backend.project_dir)}')
        backend.move_to_interim(spine_file)
        outline_file = spine_file.with_suffix('.outline')
        backend.move_to_interim(outline_file)


import os
os.environ['DISABLE_JULIA'] = '1'

from taggingbackends.main import main

if __name__ == '__main__':
    main(make_dataset)
