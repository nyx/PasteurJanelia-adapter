import logging
import subprocess
from pathlib import Path

def parse_output(default_logger, output):
    if output:
        output = output.decode('utf-8')
        filtered = []
        for line in output.splitlines():
            delegated = False
            for prefix in ('DEBUG:', 'INFO:', 'WARNING:', 'ERROR:'):
                if line.startswith(prefix):
                    # let taggingBackends.BackendExplorer handle the message
                    print(line)
                    delegated = True
                    break
            if not delegated:
                filtered.append(line)
        if filtered:
            default_logger(r'\n'.join(filtered))

def predict_model(backend, **kwargs):
    """
    Trivially locates the required components and runs the *pipeline_extract_states* executable.
    """
    path_to_classifiers = str(backend.model_dir()) + '/'
    pipeline_extract_states = str(backend.project_dir / 'bin' / 'run.sh')
    #
    for path_to_data, _ in backend.list_interim_files(group_by_directories=True).items():
        logging.info(f'processing directory: {path_to_data}')
        #
        ret = subprocess.run([pipeline_extract_states, path_to_classifiers, path_to_data], capture_output=True)
        parse_output(logging.info, ret.stdout)
        parse_output(logging.error, ret.stderr)
        #
        trxmat = Path(path_to_data) / 'trx.mat'
        if trxmat.is_file():
            backend.move_to_processed(trxmat)


import os
os.environ['DISABLE_JULIA'] = '1'

from taggingbackends.main import main

if __name__ == '__main__':
    main(predict_model)
