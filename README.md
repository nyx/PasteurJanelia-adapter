# pipeline\_extract\_states' adapter for LarvaTagger.jl

PasteurJanelia-adapter is a [tagging backend](https://gitlab.pasteur.fr/nyx/TaggingBackends) for LarvaTagger.jl. It only supports the `predict` command, *i.e.* it is not possible to train a new tagger with the same backend.

pipeline\_extract\_states is a MatLab program from [Pipeline\_action\_analysis\_t5\_pasteur\_janelia](https://github.com/DecBayComp/Pipeline_action_analysis_t5_pasteur_janelia) (modified, more information in [src/matlab](src/matlab)).

No MatLab license is required.

Install with the following command, to be run from the project's root directory:
```
make
```
Note that this installation procedure is only known to work on Linux. It installs the MatLab Runtime Engine for the glnxa64 architecture, and requires basic packages such as make, wget and unzip.

Copy Choreography tracking data files (*.spine* and *.outline* files) in the *data/raw/5layers* directory (create it if necessary), and then run (still from the project's root directory):
```
DISABLE_JULIA=1 poetry run tagging-backend predict --model-instance 5layers --make-dataset
```

A resulting *trx.mat* will be generated in the *data/processed/5layers* directory.

## Troubleshooting

If you get `version 'GLIBCXX_3.4.26' not found` error messages, you need to upgrade gcc.
On Maestro, this is done with:
```
module load gcc/9.5.0
```
and appending `:/opt/gensoft/exe/gcc/9.5.0/lib64` to the `export LD_LIBRARY_PATH=` statement in the *bin/mcrpath.sh* file.
