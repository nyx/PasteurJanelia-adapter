#!/bin/bash

# define MCR_CACHE_ROOT
if [ -z "$MCR_CACHE_ROOT" ]; then
  if [ -z "$SLURM_ARRAY_TASK_ID" ]; then
    MCR_CACHE_ROOT=~/.cache/mcr/$SLURM_ARRAY_TASK_ID
  elif [ -z "$SLURM_JOB_ID" ]; then
    MCR_CACHE_ROOT=~/.cache/mcr/$SLURM_JOB_ID
  else
    MCR_CACHE_ROOT=~/.cache/mcr/default
  fi
  export MCR_CACHE_ROOT
fi

rm -rf "$MCR_CACHE_ROOT"
mkdir -p "$MCR_CACHE_ROOT"

# define LD_LIBRARY_PATH
BIN=$(dirname ${BASH_SOURCE[0]})
source $BIN/mcrpath.sh

# run the tagger
$BIN/pipeline_extract_states $@

# clean up
rm -rf "$MCR_CACHE_ROOT"
