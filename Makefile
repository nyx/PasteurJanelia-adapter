
MATLAB_VERSION:=2023b


.PHONY: install clean package .force

install: bin/pipeline_extract_states bin/mcrpath.sh models/5layers
	poetry install --only main

models/5layers:
	cd src/matlab && $(MAKE) github-nolicense Classifier_fifth_layer_RF_compact
	mkdir -p $@
	cp -Rp src/matlab/github-nolicense/Classifier_* $@/
	cp -Rp src/matlab/Classifier_fifth_layer_RF_compact $@/

bin/mcrpath.sh: Makefile.nomcr
	$(MAKE) -f $< MATLAB_VERSION=$(MATLAB_VERSION) SH=sh $@

Makefile.nomcr:
	wget "https://gitlab.com/larvataggerpipelines/TrxmatRetagger/-/raw/main/Makefile.nomcr?ref_type=heads&inline=false" -O $@

bin/pipeline_extract_states: bin/pipeline_extract_states_$(MATLAB_VERSION) .force
	ln -srf $< $@

# Maestro only
bin/pipeline_extract_states_%:
	cd src/matlab && $(MAKE) MATLAB_VERSION=$*
	cp -Rp src/matlab/pipeline_extract_states/pipeline_extract_states $@

# override
bin/pipeline_extract_states_2023b:
	wget "https://gitlab.pasteur.fr/nyx/artefacts/-/raw/master/PasteurJanelia/pipeline_extract_states_2023b?ref_type=heads&inline=false" -O $@
	chmod a+x $@

clean:
	cd src/matlab && $(MAKE) clean
	rm -rf bin/matlab
	rm -f bin/pipeline_extract_states*
	rm -f bin/mcrpath.sh
	rm -f Makefile.nomcr
	rm -rf models
	rm -rf atleastoneoperand "$(shell poetry env info -p)"
	rm -f poetry.lock

package: install
	$(MAKE) -f Makefile.nomcr MATLAB_VERSION=$(MATLAB_VERSION) matlablighter
	rm -rf src/matlab
	rm Makefile.nomcr

.force:
